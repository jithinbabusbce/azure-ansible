# Create (and display) an SSH key
resource "tls_private_key" "ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}


output "tls_private_key" { value = tls_private_key.ssh_key.private_key_pem }

##################### Network ###################
module "network" {
  source = "../module/network/"
  env = "test"
  rgname = "testing"
  location = "canadacentral"
  network_cidr = "10.1.0.0/16"
  prefix1 = "10.1.1.0/24"
  prefix2 = "10.1.2.0/24"
  prefix3 = "10.1.3.0/24"

}

####################### NSG ######################

module "nsgoscar" {
  source = "../module/nsg/"
  env = "test"
  resource_group_name = module.network.resource_group_name
  location = "canadacentral"
  security_group_name   = "oscarnsg"
  source_address_prefix = ["10.1.0.0/16"]
  predefined_rules = [
    {
      name     = "SSH"
      priority = "500"
    },
    {
      name              = "HTTP"
      source_port_range = "80"
    }
  ]

  custom_rules = [
    {
      name                   = "myssh"
      priority               = 201
      direction              = "Inbound"
      access                 = "Allow"
      protocol               = "tcp"
      source_port_range      = "*"
      destination_port_range = "22"
      source_address_prefix  = "10.1.0.0/16"
      description            = "description-myssh"
    },
    {
      name                    = "oscarport"
      priority                = 200
      direction               = "Inbound"
      access                  = "Allow"
      protocol                = "tcp"
      source_port_range       = "*"
      destination_port_range  = "8080"
      source_address_prefix = "0.0.0.0/0"
      description             = "description-oscarport"
    },
    {
      name                    = "apiport"
      priority                = 202
      direction               = "Inbound"
      access                  = "Allow"
      protocol                = "tcp"
      source_port_range       = "*"
      destination_port_range  = "9008"
      source_address_prefix = "0.0.0.0/0"
      description             = "description-apiport"
    },
  ]

  tags = {
    environment = "oscarport"
  }
}

module "mariadb" {
  source = "../module/nsg/"
  env = "test"
  resource_group_name = module.network.resource_group_name
  location = "canadacentral"
  security_group_name   = "mariadb"
  source_address_prefix = ["10.1.0.0/16"]
  predefined_rules = [
    {
      name     = "SSH"
      priority = "500"
    },
    {
      name              = "MySQL"
      source_port_range = "3306"
    }
  ]

  custom_rules = [
    {
      name                   = "ssh-port"
      priority               = 202
      direction              = "Inbound"
      access                 = "Allow"
      protocol               = "tcp"
      source_port_range      = "*"
      destination_port_range = "*"
      source_address_prefix  = "10.1.0.0/16"
      description            = "ssh port"
    },
  ]

  tags = {
    environment = "oscarport"
  }
}

##################### VPN Server #########################

# module "VPN" {
#   source = "../module/vpn/"
#   env = "test"
#   resource_group_name = module.network.resource_group_name
#   location = "canadacentral"
#   ssh-source-address = "10.1.0.0/16"
#   subnet1 = module.network.vnet_subnet1
#   vpn_size = "Standard_B1s"
#   public_key_openssh = tls_private_key.ssh_key.public_key_openssh

# }

######################## VM ######################
# module "Oscarvm" {
#   source = "../module/private-vm/"
#   env = "test"
#   resource_group_name = module.network.resource_group_name
#   location = "canadacentral"
#   vmname = "oscar"
#   subnet1 = module.network.vnet_subnet1
#   network_security_group_id = module.nsgoscar.network_security_group_id
#   vm_size = "Standard_B1s"
#   publisher = "Canonical"
#   offer = "UbuntuServer"
#   sku = "18.04-LTS"
#   vmuser = "azureuser"
#   public_key_openssh = tls_private_key.ssh_key.public_key_openssh
# }

############# Static website ###########
module "staticwebsite" {
  source = "../module/static-website/"
  env = "test"
  resource_group_name = module.network.resource_group_name
  location = "canadacentral"
  staticwebsite_name = "innonia"
  storage_account_kind = "StorageV2"
  storage_account_tier = "Standard"
  storage_account_replication_type = "GRS"
  storage_traffic_type = "true"
}






