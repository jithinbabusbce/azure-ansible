resource "azurerm_virtual_network" "network" {
  name                = join("-", [var.env, "network"])
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = [var.network_cidr]

  tags = {
    environment = var.env
  }
}


resource "azurerm_subnet" "subnet1" {
  name                 = join("-", [var.env, "subnet1"])
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.network.name
  address_prefixes     = [var.prefix1]
}

resource "azurerm_subnet" "subnet2" {
  name                 = join("-", [var.env, "subnet2"])
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.network.name
  address_prefixes     = [var.prefix2]
}


resource "azurerm_subnet" "subnet3" {
  name                 = join("-", [var.env, "subnet3"])
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.network.name
  address_prefixes     = [var.prefix3]
}
