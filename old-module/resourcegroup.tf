resource "azurerm_resource_group" "rg" {
  name     = join("-", [var.env, "resource-group"])
  location = var.location
}