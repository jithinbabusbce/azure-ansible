variable "location" {
  type = string
}

variable "env" {
  type = string
}

variable "prefix1" {
  type = string
}

variable "prefix2" {
  type = string
}

variable "prefix3" {
  type = string
}

variable "network_cidr" {
  description = "The address space to be used for the Azure virtual network."
}

variable "zones" {
  type    = list(string)
  default = []
}

variable "ssh-source-address" {
  type = string
}

############################ Oscar VM Related ############################

variable "vm_size" {
  type = string
}

variable "scale_set_image" {
  type = string
}

variable "vm_tier" {
  type = string
}

variable "oscar_capacity" {
  type = string
}

variable "oscar_max_capacity" {
  type = string
}


####################

variable "vpn_size" {
  type = string
}

################ Storage ##############3
  
variable "static_website_name" {
  type = string
}

variable "tfstatefilename" {
  type = string
}

variable "oscar_storage_name" {
  type = string
}

variable "oscar_share_size" {
  type = string
}

##################### Application Gateway #######################

variable "gateway_size" {
  type = string
}

variable "firewall_mode" {
  type = string
}