resource "azurerm_public_ip" "vpn" {
  name                = join("-", [var.env, "vpn"])
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"

  tags = {
    environment = var.env
  }
}

resource "azurerm_network_interface" "vpn" {
  name                = join("-", [var.env, "vpn"])
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = join("-", [var.env, "vpn"])
    subnet_id                     = azurerm_subnet.subnet1.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.vpn.id
  }
}

resource "azurerm_network_interface_security_group_association" "instance" {
  network_interface_id      = azurerm_network_interface.vpn.id
  network_security_group_id = azurerm_network_security_group.vpn-nsg.id
}


######################## VM Creation #############

# Generate random text for a unique storage account name
resource "random_id" "randomId" {
  keepers = {
    # Generate a new ID only when a new resource group is defined
    resource_group = azurerm_resource_group.rg.name
  }

  byte_length = 8
}

# Create storage account for boot diagnostics
resource "azurerm_storage_account" "storageaccount" {
  name                     = "diag${random_id.randomId.hex}"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = var.env
  }
}

# Create virtual machine
resource "azurerm_linux_virtual_machine" "vpn" {
  name                  = join("-", [var.env, "vpn"])
  location              = var.location
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.vpn.id]
  size                  = var.vpn_size

  os_disk {
    name                 = join("-", [var.env, "vpn-myOsDisk"])
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  plan {
    publisher = "openvpn"
    product   = "openvpnas"
    name      = "access_server_byol"
  }

  source_image_reference {
    publisher = "openvpn"
    offer     = "openvpnas"
    sku       = "access_server_byol"
    version   = "285.0.0"
  }

  computer_name                   = join("-", [var.env, "vpn"])
  admin_username                  = "azureuser"
  disable_password_authentication = true

  admin_ssh_key {
    username   = "azureuser"
    public_key = tls_private_key.ssh_key.public_key_openssh
  }


  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.storageaccount.primary_blob_endpoint
  }

  tags = {
    environment = var.env
  }
}