variable "location" {
  type = string
}

variable "env" {
  type = string
}

variable "vmname" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "subnet1" {
  type = string
}

variable "network_security_group_id" {
  type = string
}

variable "vm_size" {
  type = string
}

variable "publisher" {
  type = string
}

variable "offer" {
  type = string
}

variable "sku" {
  type = string
}

variable "vmuser" {
  type = string
}

variable "public_key_openssh" {
  type = string
}
