resource "azurerm_network_interface" "vm" {
  name                = join("-", [var.env, var.vmname])
  location            = var.location
  resource_group_name = var.resource_group_name

  ip_configuration {
    name                          = join("-", [var.env, var.vmname])
    subnet_id                     = var.subnet1
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_network_interface_security_group_association" "instance" {
  network_interface_id      = azurerm_network_interface.vm.id
  network_security_group_id = var.network_security_group_id
}


# Generate random text for a unique storage account name
resource "random_id" "randomId" {
  keepers = {
    # Generate a new ID only when a new resource group is defined
    resource_group = var.resource_group_name
  }

  byte_length = 8
}


# Create storage account for boot diagnostics
resource "azurerm_storage_account" "vmstorageaccount" {
  name                     = "diag${random_id.randomId.hex}"
  resource_group_name      = var.resource_group_name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = var.env
  }
}




resource "azurerm_virtual_machine" "main" {
  name                  = join("-", [var.env, var.vmname])
  location              = var.location
  resource_group_name   = var.resource_group_name
  network_interface_ids = [azurerm_network_interface.vm.id]
  size                  = var.vm_size


  storage_image_reference {
    publisher = var.publisher
    offer     = var.offer
    sku       = var.sku
    version   = "latest"
  }

  storage_os_disk {
    name                 = join("-", [var.env, var.vmname])
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

 
   computer_name                   = join("-", [var.env, var.vmname])
   admin_username                  = var.vmuser
   disable_password_authentication = true

  admin_ssh_key {
    username   = var.vmuser
    public_key = var.public_key_openssh
  }

  tags = {
    environment = var.env
  }


}