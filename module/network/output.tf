output "vnet_id" {
  description = "The id of the newly created vNet"
  value       = azurerm_virtual_network.network.id
}

output "vnet_subnet1" {
  description = "The ids of subnets created inside the newl vNet"
  value       = azurerm_subnet.subnet1.id
}

output "vnet_subnet2" {
  description = "The ids of subnets created inside the newl vNet"
  value       = azurerm_subnet.subnet2.id
}

output "vnet_subnet3" {
  description = "The ids of subnets created inside the newl vNet"
  value       = azurerm_subnet.subnet3.id
}

output "resource_group_name" {
  description = "The name of the resource group in which resources are created"
  value       = azurerm_resource_group.rg.name
}