variable "location" {
  type = string
}

variable "env" {
  type = string
}

variable "rgname" {
  type = string
}

variable "prefix1" {
  type = string
}

variable "prefix2" {
  type = string
}

variable "prefix3" {
  type = string
}

variable "network_cidr" {
  description = "The address space to be used for the Azure virtual network."
}

variable "zones" {
  type    = list(string)
  default = []
}
