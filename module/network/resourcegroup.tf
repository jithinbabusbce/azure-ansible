resource "azurerm_resource_group" "rg" {
  name     = join("-", [var.env, var.rgname])
  location = var.location
}