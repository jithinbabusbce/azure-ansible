variable "location" {
  type = string
}

variable "env" {
  type = string
}

# Network Security Group definition
variable "resource_group_name" {
  description = "Name of the resource group"
  type        = string
}


variable "staticwebsite_name" {
  type = string
}

variable "storage_account_kind" {
  type = string
}

variable "storage_account_tier" {
  type = string
}

variable "storage_account_replication_type" {
  type = string
}

variable "storage_traffic_type" {
  type = string
}




