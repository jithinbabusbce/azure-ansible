resource "azurerm_storage_account" "static_storage" {
  name                     = var.staticwebsite_name
  resource_group_name      = var.resource_group_name
  location                 = var.location
  account_kind             = var.storage_account_kind
  account_tier             = var.storage_account_tier
  account_replication_type = var.storage_account_replication_type
  enable_https_traffic_only = var.storage_traffic_type

  static_website {
    index_document = "index.html"
  }

  tags = {
    env = var.env
  }
}