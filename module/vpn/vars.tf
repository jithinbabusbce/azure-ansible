variable "location" {
  type = string
}

variable "env" {
  type = string
}

# Network Security Group definition
variable "resource_group_name" {
  description = "Name of the resource group"
  type        = string
}


variable "ssh-source-address" {
  type = string
}

variable "subnet1" {
  type = string
}

variable "vpn_size" {
  type = string
}

variable "public_key_openssh" {
  type = string
}






